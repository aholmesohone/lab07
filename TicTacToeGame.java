import java.util.Scanner;
public class TicTacToeGame{
	
    public static void main(String[] args){
	
	System.out.println("Welcome to a game of Tic-Tac-Toe, the greatest game to ever exist");
	System.out.println("What do you mean \"thats exactly what a Tic-Tac-Toe program would say\"");
	
	
	Scanner scan = new Scanner(System.in);
	Board bord = new Board();
	boolean gameOver = false;
	int player = 1;
	Square playerToken = Square.X;
	
	
		while(gameOver == false){
			
			System.out.println(bord);
			
			if(player == 1){
				playerToken = Square.X;
			}
			else{
				playerToken = Square.O;
			}
			
			System.out.println("It is player "+player+"'s turn.");
			
			int row = 0;
			int column = 0;
			boolean valid = false;
			
			while(valid == false){
				
				row = scan.nextInt();
				column = scan.nextInt();
			
				valid =(bord.placeToken(row,column,playerToken));
				if(valid==false){
					System.out.println("Invalid Location, please re-enter");
				}
			}
		
			if(bord.checkIfWinning(playerToken) == true){
				System.out.println("Player "+player+" has won the game!");
				gameOver = true;
			}
			else if(bord.checkFull() == true){
				System.out.println("The board is full, It's a tie!");
				gameOver = true;
			}
			else{
				player++;
				if(player >2){
					player =1;
				}
			}
		
		}
		
		System.out.println(bord);
	
	}	
}