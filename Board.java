public class Board{
    private Square[][] tictactoeBoard = new Square[3][];

    public Board(){
		for(int i=0; i<3; i++){
			this.tictactoeBoard[i] = new Square[3];
			for(int j=0; j<3; j++){
				this.tictactoeBoard[i][j] = Square.BLANK;
			}
		}
		
    }
	
	public String toString(){
		String output = "  0 1 2\n";
		
		
		for(int i=0; i<3; i++){
			output += i+" "; //makes the vertical 0 1 2
			for(int j=0; j<3; j++){
				output += this.tictactoeBoard[i][j];
				
				if(j!= 2){
					output += " ";
				}
			}
			output += "\n";
		}
		
		return output;
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
		
		if(row > 2 && col > 2 && row < 0 && col < 0){
			return false;
		}
		
		if(this.tictactoeBoard[row][col] == Square.BLANK){
			this.tictactoeBoard[row][col] = playerToken;
			return true;
		}
		
		return false;
		
	}
	
	public boolean checkFull(){
		
		for(int i=0; i<3; i++){
			for(int j=0; j<3; j++){
				if(this.tictactoeBoard[i][j] == Square.BLANK){
					return false;
				}
			}
		}
		return true;
		
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		boolean win = false;
		
		for(int i=0; i<3; i++){
			win = true;
			
			for(int j=0; j<3; j++){
				if(this.tictactoeBoard[i][j] != playerToken){
					win = false;
				}
			}
			
			if(win == true){
				return true;
			}
			
		}
		return false;
		
	}
	
	private boolean checkIfWinningVertical(Square playerToken){
		boolean win = false; 
		
		for(int i=0; i<3; i++){		
			win = true;
			
			for(int j=0; j<3; j++){ 
				if(this.tictactoeBoard[j][i] != playerToken){
					win = false;
				}
			}
			
			if(win == true){
				return true;
			}
			
		}
		return false;
	} 
	
	private boolean checkIfWinningDiagonal(Square playerToken){
		boolean win = true;

		// "\" diagonal
		for(int i=0; i<3; i++){ 
			
			if(this.tictactoeBoard[i][i] != playerToken){
					win = false;
			}
			
		}
		
		if(win == true){
			return true;
		}
		
		// "/" diagonal
		win = true;
		int j=2;
		
		for(int i=0; i<3; i++){ 
			
			if(this.tictactoeBoard[i][j] != playerToken){
					win = false;
			}
			
			j--;
			
		}
		
		if(win == true){
			return true;
		}
		
		return false;
	}
	
	public boolean checkIfWinning(Square playerToken){
		
		if(checkIfWinningVertical(playerToken) == true){
			return true;
		}
		
		if(checkIfWinningHorizontal(playerToken) == true){
			return true;
		}
		
		if(checkIfWinningDiagonal(playerToken) == true){
			return true;
		}
		return false;
	}
	
	
}

/*
        this.first = new Die();
        this.second = new Die();
		for(int i=0; i<12; i++){
			this.tiles[i] = false;
		}

*/